package com.example.ars.colorpicker;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ColorPickActivity extends AppCompatActivity {

    private RelativeLayout colorLayout;
    private TextView colorTextView;
    private ColorButton colorButton;
    private ArrayList<ColorButton> colorButtons;
    private FavoriteColorsGridAdapter favoriteColorsGridAdapter;
    private TextView hsvTextView;
    private TextView rgbTextView;
    private LinearLayout bottomLayout;
    private ListenerHelper listenerHelper;
    private Resources resources;
    private CheckBox favoriteCheckBox;
    private final static String SELECTED_KEY = "selected";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_pick);
        ImageButton okButton = (ImageButton) findViewById(R.id.okButton);
        final DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        colorButtons = new ArrayList<>();
        resources = getResources();
        final ColorPickActivityControl colorPickActivityControl = getColorPickActivityControl();
        colorLayout = (RelativeLayout) findViewById(R.id.colorLayout);
        colorTextView = (TextView) findViewById(R.id.colorTextView);
        favoriteCheckBox = (CheckBox) findViewById(R.id.favoriteCheckBox);
        LinearLayout layout = (LinearLayout) findViewById(R.id.colorPick);
        GridView grid = (GridView) findViewById(R.id.GridView);
        grid.setColumnWidth((int) (resources.getDimension(R.dimen.colorPickMiniature) + resources.getDimension(R.dimen.colorPickMargin) * 2));
        Object lastNonConfigurationInstance = getLastCustomNonConfigurationInstance();
        Cursor c = db.query(DBHelper.TABLE_FAVORITES, null, null, null, null, null, null);
        favoriteColorsGridAdapter = new FavoriteColorsGridAdapter(this, c);

        listenerHelper = new ListenerHelper(
                this,
                dbHelper,
                (Vibrator) getSystemService(VIBRATOR_SERVICE),
                (LockableScrollView) findViewById(R.id.horizontalScrollView),
                (RelativeLayout) findViewById(R.id.relativeLayoutEditMode),
                (FrameLayout) findViewById(R.id.exampleLayout), favoriteColorsGridAdapter, favoriteCheckBox, colorPickActivityControl);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                favoriteColorsGridAdapter.setSingle(position);;
                ColorButton colorButton = favoriteColorsGridAdapter.get(position);
                colorPickActivityControl.UpdateColorButton(colorButton);
                colorPickActivityControl.UpdateBackgroundColor(colorButton.getColor());
                favoriteCheckBox.setChecked(true);
                favoriteColorsGridAdapter.notifyDataSetChanged();
            }
        });

        if (lastNonConfigurationInstance != null) {
            addButtons(layout, (ArrayList<ColorButton>) lastNonConfigurationInstance);
        } else {
            addButtons(layout, 16);
        }

        grid.setNumColumns(GridView.AUTO_FIT);
        grid.setAdapter(favoriteColorsGridAdapter);
        rgbTextView = (TextView) findViewById(R.id.rgbTextView);
        hsvTextView = (TextView) findViewById(R.id.hsvTextView);
        bottomLayout = (LinearLayout) findViewById(R.id.relativeLayout);
        favoriteCheckBox.setOnClickListener(listenerHelper.getOnClickListenerFavoriteCheckBox());

        Intent intent = getIntent();

        int color = intent.getIntExtra(NoteInfo.KEY_COLOR, Color.GRAY);
        ColorButton button = new ColorButton(this, color, 0, 360);
        colorPickActivityControl.UpdateColorButton(button);
        colorPickActivityControl.UpdateBackgroundColor(color);
        if(dbHelper.containsColor(color)){
            favoriteCheckBox.setChecked(true);
            favoriteColorsGridAdapter.setSingle(favoriteColorsGridAdapter.getPosition(color));
        }

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(NoteInfo.KEY_COLOR, colorButton.getColor());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @NonNull
    private ColorPickActivityControl getColorPickActivityControl() {
        return new ColorPickActivityControl() {
            private boolean editMode = false;

            @Override
            public void SwitchEditMode(boolean mode, ColorButton button) {
                editMode = mode;
            }

            @Override
            public boolean IsEditMode() {
                return editMode;
            }

            @Override
            public void UpdateColorButton(ColorButton button) {
                colorButton = button;
            }

            @Override
            public void UpdateBackgroundColor(int color) {
                updateBackgroundColor(color);
            }

            @Override
            public ColorButton GetColorButton() {
                return colorButton;
            }
        };
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!favoriteColorsGridAdapter.selected.isEmpty())
            outState.putInt(SELECTED_KEY, favoriteColorsGridAdapter.selected.get(0));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(SELECTED_KEY)) {
            favoriteCheckBox.setEnabled(true);
            favoriteColorsGridAdapter.setSingle(savedInstanceState.getInt(SELECTED_KEY));
            favoriteColorsGridAdapter.notifyDataSetChanged();
            colorButton = favoriteColorsGridAdapter.getFirstSelected();
            updateBackgroundColor(colorButton.getColor());
        }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return colorButtons;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LinearLayout layout = (LinearLayout) findViewById(R.id.colorPick);
        layout.removeAllViews();
    }

    private void addButtons(final LinearLayout layout, ArrayList<ColorButton> buttons) {
        int count = buttons.size();
        final int[] colors = new int[count + 2];
        final float[] position = new float[count + 2];
        float widthButton = resources.getDimension(R.dimen.colorPickSize) + resources.getDimension(R.dimen.colorPickMargin) * 2;
        float width = widthButton * count;
        float pos = 0.0f;
        colors[0] = buttons.get(0).getColor();
        position[0] = pos;
        pos += widthButton / 2;
        for (int i = 0; i < count; i++) {
            ColorButton button = buttons.get(i);
            button.setOnClickListener(listenerHelper.getOnClickListener(button));
            button.setOnLongClickListener(listenerHelper.getLongClickListener(button));
            button.setOnTouchListener(listenerHelper.getOnTouchListener(button));
            colorButtons.add(button);
            layout.addView(button);
            colors[i] = button.getSourceColor();
            position[i] = pos / width;
            pos += widthButton;
        }
        pos -= widthButton / 2;
        position[count - 1] = pos;
        colors[count - 1] = buttons.get(count - 1).getColor();

        setBackground(layout, colors, position);
    }

    private void addButtons(final LinearLayout layout, int count) {
        final int[] colors = new int[count + 2];
        final float[] position = new float[count + 2];
        float[] hsv = {0.0f, 1.0f, 1.0f};
        float pos = 0.0f;
        colors[0] = Color.HSVToColor(hsv);
        position[0] = pos;
        float widthButton = resources.getDimension(R.dimen.colorPickSize) + resources.getDimension(R.dimen.colorPickMargin) * 2;
        float width = widthButton * count;
        pos += widthButton / 2;
        float shift = 360.0f / count;
        for (int i = 0; i < count; i++) {
            final int color = Color.HSVToColor(hsv);
            colors[i + 1] = color;
            position[i + 1] = pos / width;
            final ColorButton button = CreateButton(layout, color, hsv[0] - shift / 2, hsv[0] + shift / 2);
            button.setOnClickListener(listenerHelper.getOnClickListener(button));
            button.setOnLongClickListener(listenerHelper.getLongClickListener(button));
            button.setOnTouchListener(listenerHelper.getOnTouchListener(button));
            colorButtons.add(button);
            hsv[0] += shift;
            pos += widthButton;
        }
        pos -= widthButton / 2;
        colors[count + 1] = Color.HSVToColor(hsv);
        position[count + 1] = pos / width;
        setBackground(layout, colors, position);
    }

    private void updateBackgroundColor(int color) {
        float[] colorHSVLight = new float[3];
        Color.colorToHSV(color, colorHSVLight);
        hsvTextView.setText(String.format(" (%s, %.2f, %.2f)", (int) colorHSVLight[0], colorHSVLight[1], colorHSVLight[2]));
        rgbTextView.setText(String.format(" (%s, %s, %s)", Color.red(color), Color.green(color), Color.blue(color)));
        colorHSVLight[1] -= 0.8;
        colorHSVLight[2] += 0.8;
        int colorLight = Color.HSVToColor(colorHSVLight);
        GradientDrawable gradient = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{colorLight, color});
        bottomLayout.setBackgroundColor(colorLight);
        colorLayout.setBackground(gradient);
        colorTextView.setTextColor(colorLight);
    }

    private void setBackground(LinearLayout layout, final int[] colors, final float[] position) {
        ShapeDrawable.ShaderFactory sf = new ShapeDrawable.ShaderFactory() {
            @Override
            public Shader resize(int width, int height) {
                return new LinearGradient(0, 0, width, height,
                        colors,
                        position, Shader.TileMode.CLAMP);
            }
        };
        PaintDrawable p = new PaintDrawable();
        p.setShape(new RectShape());
        p.setShaderFactory(sf);
        layout.setBackground(p);
    }

    private ColorButton CreateButton(LinearLayout layout, int color, float left, float right) {
        ColorButton button = new ColorButton(this, color, left, right);
        layout.addView(button);
        return button;
    }
}
