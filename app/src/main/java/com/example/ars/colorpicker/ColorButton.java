package com.example.ars.colorpicker;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.widget.LinearLayout;

public class ColorButton extends android.support.v7.widget.AppCompatButton {

    private final Drawable buttonDrawable;
    private int color;
    private int sourceColor;
    private float leftLimit;
    private float rightLimit;
    private boolean isFavorite;
    public float[] currentColor;

    public ColorButton(Context context, int color, float leftLimit, float rightLimit) {
        super(context);
        this.color = color;
        this.sourceColor = color;
        this.leftLimit = leftLimit;
        this.rightLimit = rightLimit;
        this.isFavorite = false;
        currentColor = new float[3];
        Color.colorToHSV(color, currentColor);
        LinearLayout.LayoutParams buttonParams = getButtonParams();
        this.setLayoutParams(buttonParams);
        buttonDrawable = getResources().getDrawable(R.drawable.circle_button);
        this.setBackground(buttonDrawable);
        setBackgroundColor(color);
    }

    public int getColor() {
        return Color.HSVToColor(currentColor);
    }

    public int getSourceColor() {
        return sourceColor;
    }

    public void returnSourceColor() {
        this.color = sourceColor;
        Color.colorToHSV(this.color, currentColor);
        setBackgroundColor(sourceColor);
    }

    @Override
    public void setBackgroundColor(@ColorInt int color) {
        buttonDrawable.setColorFilter(color, PorterDuff.Mode.SRC);
        this.setBackground(buttonDrawable);
    }

    public float getLeftLimit() {
        return leftLimit;
    }

    public float getRightLimit() {
        return rightLimit;
    }


    @NonNull
    private LinearLayout.LayoutParams getButtonParams() {
        Resources resources = getResources();
        LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        buttonParams.width = (int) resources.getDimension(R.dimen.colorPickSize);
        buttonParams.height = (int) resources.getDimension(R.dimen.colorPickSize);
        int margin = (int) resources.getDimension(R.dimen.colorPickMargin);
        buttonParams.leftMargin = margin;
        buttonParams.topMargin = margin;
        buttonParams.rightMargin = margin;
        buttonParams.bottomMargin = margin;
        return buttonParams;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }


    public void changeColor(float shiftX, float shiftY) {
        currentColor = getChangeColor(currentColor, shiftX, shiftY);
        int color = Color.HSVToColor(currentColor);
        this.color = color;
        setBackgroundColor(color);
    }

    public float[] getChangeColor(float[] color, float shiftX, float shiftY) {
        float[] newColor = color.clone();
        if (shiftX != 0) {
            if (newColor[0] + shiftX <= rightLimit && newColor[0] + shiftX >= leftLimit) {
                color[0] += shiftX;
            } else {
                if (newColor[0] + shiftX > rightLimit) newColor[0] = rightLimit;
                if (newColor[0] + shiftX < leftLimit) newColor[0] = leftLimit;
            }
        }
        if (shiftY != 0) {
            if (newColor[2] + shiftY / 50 <= 1 && newColor[2] + shiftY / 50 >= 0) {
                newColor[2] += shiftY / 50;
            } else {
                if (newColor[2] + shiftY / 50 > 1) newColor[2] = 1;
                if (newColor[2] + shiftY / 50 < 0) newColor[2] = 0;
            }
        }
        currentColor = newColor;
        this.color = Color.HSVToColor(newColor);
        setBackgroundColor(this.color);

        return newColor;
    }

    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
