package com.example.ars.colorpicker;

import android.os.Bundle;
import android.util.Pair;

import java.util.ArrayList;

class DBQueryHelper{
    static String getSortByFromBundle(Bundle savedInstanceState)
    {
        String sortBy = savedInstanceState.getString(SearchActivity.KEY_SORT_BY);
        String desc = "";
        if (savedInstanceState.getBoolean(SearchActivity.KEY_SORT_DIRECTION)) {
            desc += " DESC";
        }

        return sortBy + desc;
    }

    static Pair<String, String[]> getQueryFromBundle(Bundle savedInstanceState)
    {
        String query = "";
        ArrayList<String> args = new ArrayList<>();
        String filterBy = savedInstanceState.getString(SearchActivity.KEY_TYPE_FILTER);

        long from = savedInstanceState.getLong(SearchActivity.KEY_DATE_FROM, 0);
        if (from != 0) {
            query += filterBy + " > ?";
            args.add(Long.toString(from));
        }
        long to = savedInstanceState.getLong(SearchActivity.KEY_DATE_TO, 0);
        if (to != 0) {
            if (!query.isEmpty())
                query += " AND ";
            query += filterBy + " < ?";
            args.add(Long.toString(from));
        }
        long date = savedInstanceState.getLong(SearchActivity.KEY_DATE, 0);
        if ((from == 0 && to == 0) && date != 0) {
            query += filterBy + " > ? AND " + filterBy + " < ?";
            args.add(Long.toString(date));
            args.add(Long.toString(date + 24 * 60 * 60 * 1000));
        }

        String pattern = savedInstanceState.getString(SearchActivity.KEY_PATTERN);
        if (pattern != null) {
            if (!query.isEmpty())
                query += " AND ";
            query += "(" + DBHelper.COLUMN_LABEL + " REGEXP ? OR " + DBHelper.COLUMN_TEXT + " REGEXP ?)";
            args.add(pattern + "*");
            args.add(pattern + "*");
        }

        if (savedInstanceState.containsKey(SearchActivity.KEY_COLOR)) {
            ArrayList<Integer> colors = savedInstanceState.getIntegerArrayList(SearchActivity.KEY_COLOR);
            if (!query.isEmpty())
                query += " AND ";
            query += "(";
            String colorsQuery = "";
            for (int i = 0; i < colors.size() - 1; i++) {
                colorsQuery += DBHelper.COLUMN_COLOR + " = ? " + " OR ";
                args.add(Integer.toString(colors.get(i)));
            }
            int lastColor = colors.get(colors.size() - 1);
            colorsQuery += DBHelper.COLUMN_COLOR + " = ? " + ")";
            query += colorsQuery;
            args.add(Integer.toString(lastColor));
        }

        String[] argsArray = new String[args.size()];

        args.toArray(argsArray);
        return new Pair<>(query, argsArray);
    }
}
