package com.example.ars.colorpicker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

class NoteAdapter extends CursorAdapter
{
    NoteAdapter(Context context, Cursor cursor)
    {
        super(context, cursor, 0);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        NoteHolder holder = (NoteHolder) view.getTag();

        final int id = cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_ID));
        final String label = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_LABEL));
        final String text = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_TEXT));
        final int color = cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_COLOR));
        final long dateCreate = cursor.getLong(cursor.getColumnIndex(DBHelper.COLUMN_DATE_CREATE));
        final long dateEdit = cursor.getLong(cursor.getColumnIndex(DBHelper.COLUMN_DATE_EDITING));
        final long dateView = cursor.getLong(cursor.getColumnIndex(DBHelper.COLUMN_DATE_VIEW));

        holder.label.setText(label);
        holder.text.setText(text);
        Drawable drawable = holder.color.getBackground();
        drawable.setColorFilter(color, PorterDuff.Mode.SRC);
        holder.color.setBackground(drawable);
        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditActivity.class);
                intent.putExtra(NoteInfo.KEY_LABEL, label);
                intent.putExtra(NoteInfo.KEY_TEXT, text);
                intent.putExtra(NoteInfo.KEY_COLOR, color);
                intent.putExtra(NoteInfo.KEY_DATE_CREATE, dateCreate);
                intent.putExtra(NoteInfo.KEY_DATE_EDIT, dateEdit);
                intent.putExtra(NoteInfo.KEY_DATE_VIEW, dateView);
                intent.putExtra(NoteInfo.KEY_ID, id);
                intent.putExtra(MainActivity.KEY_REQUEST_CODE, MainActivity.EDIT_NOTE);
                ((Activity)context).startActivityForResult(intent, MainActivity.EDIT_NOTE);
            }
        });
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.info_list_item, null, true);
        NoteHolder holder = new NoteHolder();
        holder.label = (TextView) view.findViewById(R.id.label);
        holder.text = (TextView) view.findViewById(R.id.text);
        holder.color = (ImageView) view.findViewById(R.id.color);
        holder.editButton = (ImageButton) view.findViewById(R.id.editButton);
        view.setTag(holder);

        return view;
    }

    NoteInfo get(int position)
    {
        Cursor cursor = getCursor();
        NoteInfo noteInfo = null;
        if(cursor.moveToPosition(position)) {
            noteInfo = new NoteInfo(cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_LABEL)),
                    cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_TEXT)),
                    cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_COLOR)),
                    cursor.getLong(cursor.getColumnIndex(DBHelper.COLUMN_DATE_CREATE)));
            noteInfo.setDateEditing(cursor.getLong(cursor.getColumnIndex(DBHelper.COLUMN_DATE_EDITING)));
            noteInfo.setDateView((cursor.getLong(cursor.getColumnIndex(DBHelper.COLUMN_DATE_VIEW))));
            int id = cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_ID));
            noteInfo.setId(id);
        }

        return noteInfo;
    }

    private class NoteHolder
    {
        TextView label;
        TextView text;
        ImageView color;
        ImageButton editButton;
    }
}
