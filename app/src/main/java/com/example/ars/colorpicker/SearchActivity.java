package com.example.ars.colorpicker;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import layout.TabFilter;
import layout.TabSearch;

public class SearchActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;


    private ViewPager mViewPager;

    public static final String KEY_DATE = "date";
    public static final String KEY_DATE_FROM = "dateFrom";
    public static final String KEY_DATE_TO = "dateTo";
    public static final String KEY_TYPE_FILTER = "typeFilter";
    public static final String KEY_PATTERN = "pattern";
    public static final String KEY_COLOR = "color";
    public static final String KEY_SORT_BY = "sortBy";
    public static final String KEY_SORT_DIRECTION = "direction";
    public static final String KEY_CHECKED_COLORS = "checkedColors";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        findViewById(R.id.buttonSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchOptions searchOptions = getOptions();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra(KEY_DATE_FROM, searchOptions.DateFrom);
                intent.putExtra(KEY_DATE_TO, searchOptions.DateTo);
                intent.putExtra(KEY_DATE, searchOptions.Date);
                if(searchOptions.Color != null)
                    intent.putExtra(KEY_COLOR, searchOptions.Color);
                intent.putExtra(KEY_TYPE_FILTER, searchOptions.FilterBy);
                intent.putExtra(KEY_SORT_BY, searchOptions.SortBy);
                intent.putExtra(KEY_SORT_DIRECTION, searchOptions.Direction);
                intent.putExtra(KEY_PATTERN, searchOptions.SearchPattern);
                startActivity(intent);
                finish();
            }
        });
    }

    public SearchOptions getOptions()
    {
        SearchOptions searchOptions = new SearchOptions();
        TabSearch tabSearch = mSectionsPagerAdapter.TabSearch;
        TabFilter tabFilter = mSectionsPagerAdapter.TabFilter;
        if(tabFilter != null){
            if (!tabFilter.dateFrom.getText().toString().isEmpty() || !tabFilter.dateTo.getText().toString().isEmpty()) {
                if (!tabFilter.dateFrom.getText().toString().isEmpty()) {
                    searchOptions.DateFrom = getReformedDate(tabFilter.dateFrom.getText().toString());
                }
                if (!tabFilter.dateTo.getText().toString().isEmpty()) {
                    searchOptions.DateTo = getReformedDate(tabFilter.dateTo.getText().toString());
                }
            } else {
                if (!tabFilter.date.getText().toString().isEmpty()) {
                    searchOptions.Date = getReformedDate(tabFilter.date.getText().toString());
                }
            }
            ArrayList<Integer> colors = tabFilter.getColors();
            if(colors != null && !colors.isEmpty()) {
                searchOptions.setColors(colors);
            }
            searchOptions.FilterBy = tabFilter.getTypeFilter();
        }
        if(tabSearch != null)
        {
            String string = tabSearch.searchEditText.getEditText().getText().toString();
            if(!string.isEmpty())
                searchOptions.SearchPattern = string;
            searchOptions.SortBy = tabSearch.getTypeSearch();
            searchOptions.Direction = tabSearch.direction.isChecked();
        }
        return searchOptions;
    }

    public long getReformedDate(String date) {
        Date parse;
        try {
            parse = NoteInfo.dateEditTextFormat.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }

        return parse.getTime();
    }



    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public TabSearch TabSearch = null;
        public TabFilter TabFilter = null;

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    TabSearch = new TabSearch();
                    return TabSearch;
                case 1:
                    TabFilter = new TabFilter();
                    return TabFilter;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SEARCH";
                case 1:
                    return "FILTER";
            }
            return null;
        }
    }
}
