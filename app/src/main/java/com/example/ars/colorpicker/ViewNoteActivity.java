package com.example.ars.colorpicker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.TextView;

public class ViewNoteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_note);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String label = extras.getString(NoteInfo.KEY_LABEL);
        String text = extras.getString(NoteInfo.KEY_TEXT);
        int color = extras.getInt(NoteInfo.KEY_COLOR);

        setTitle(label);
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(text);
        FrameLayout frameLayoutColor = (FrameLayout) findViewById(R.id.colorView);
        frameLayoutColor.setBackgroundColor(color);
    }
}
