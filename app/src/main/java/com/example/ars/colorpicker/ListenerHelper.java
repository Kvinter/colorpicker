package com.example.ars.colorpicker;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

class ListenerHelper {
    private final SQLiteDatabase db;
    private final DBHelper dbHelper;
    private Context context;
    private Vibrator vibrator;
    private LockableScrollView lockableScrollView;
    private RelativeLayout layoutEditMode;
    private FrameLayout exampleLayout;
    private FavoriteColorsGridAdapter favoriteColorsGridAdapter;
    private CheckBox favoriteCheckBox;
    private ColorPickActivityControl colorPickControl;

    ListenerHelper(
            Context context,
            DBHelper dbHelper,
            Vibrator vibrator,
            LockableScrollView lockableScrollView,
            RelativeLayout layoutEditMode,
            FrameLayout exampleLayout,
            FavoriteColorsGridAdapter favoriteColorsGridAdapter,
            CheckBox favoriteCheckBox,
            ColorPickActivityControl colorPickControl) {
        this.context = context;
        this.dbHelper = dbHelper;
        this.vibrator = vibrator;
        this.lockableScrollView = lockableScrollView;
        this.layoutEditMode = layoutEditMode;
        this.exampleLayout = exampleLayout;
        this.favoriteColorsGridAdapter = favoriteColorsGridAdapter;
        this.favoriteCheckBox = favoriteCheckBox;
        this.colorPickControl = colorPickControl;

        db = dbHelper.getWritableDatabase();
    }

    View.OnLongClickListener getLongClickListener(final ColorButton button) {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (vibrator.hasVibrator())
                    vibrator.vibrate(200);
                colorPickControl.SwitchEditMode(true, button);
                lockableScrollView.setScrollingEnabled(false);
                layoutEditMode.setVisibility(View.VISIBLE);
                Drawable drawable = exampleLayout.getBackground();
                drawable.setColorFilter(button.getColor(), PorterDuff.Mode.SRC);
                exampleLayout.setBackground(drawable);
                return true;
            }
        };
    }

    View.OnClickListener getOnClickListener(final ColorButton button) {
        return new View.OnClickListener() {
            private static final long DOUBLE_PRESS_INTERVAL = 200;
            private long lastPressTime;
            private boolean hasDoubleClicked = false;


            @Override
            public void onClick(View v) {
                long pressTime = System.currentTimeMillis();
                if (pressTime - lastPressTime <= DOUBLE_PRESS_INTERVAL) {
                    hasDoubleClicked = true;
                    button.returnSourceColor();
                } else {
                    hasDoubleClicked = false;
                    Handler handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (!hasDoubleClicked) {
                                if (!favoriteCheckBox.isEnabled())
                                    favoriteCheckBox.setEnabled(true);
                                colorPickControl.UpdateColorButton(button);
                                favoriteCheckBox.setChecked(button.isFavorite());
                                if(dbHelper.containsColor(button.getColor())){
                                    colorPickControl.UpdateColorButton(button);
                                    favoriteCheckBox.setChecked(true);
                                    favoriteColorsGridAdapter.setSingle(favoriteColorsGridAdapter.getPosition(button.getColor()));
                                    favoriteColorsGridAdapter.notifyDataSetChanged();
                                }
                                else {
                                    favoriteCheckBox.setChecked(false);
                                    favoriteColorsGridAdapter.selected.clear();;
                                    favoriteColorsGridAdapter.notifyDataSetChanged();
                                }
                                colorPickControl.UpdateBackgroundColor(button.getColor());
                            }
                        }
                    };
                    handler.sendEmptyMessageDelayed(0, DOUBLE_PRESS_INTERVAL);
                }
                lastPressTime = pressTime;
            }
        };
    }

    View.OnTouchListener getOnTouchListener(final ColorButton button) {
        return new View.OnTouchListener() {
            Float oldX = null;
            Float oldY = null;
            Float cX = null;
            Float cY = null;
            float[] startColor = new float[3];
            float e = 10;

            private float getShiftX(float x) {
                if (cX + e > x && x > cX - e)
                    return 0;
                return (x - oldX + Math.signum(oldX - x)*e) / 40;
            }

            private float getShiftY(float y) {
                if (cY + e > y && y > cY - e)
                    return 0;
                return (y - oldY + Math.signum(oldY - y)*e) / 8;
            }

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float x = 0;
                float y = 0;
                if (colorPickControl.IsEditMode()) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_MOVE:
                            x = event.getX();
                            y = event.getY();
                            if (oldX == null || oldY == null) {
                                oldX = cX = x;
                                oldY = cY = y;
                                startColor = button.currentColor;
                            }

                            float[] c = button.getChangeColor(startColor, getShiftX(x), getShiftY(y));
                            Drawable drawable = exampleLayout.getBackground();
                            drawable.setColorFilter(Color.HSVToColor(c), PorterDuff.Mode.SRC);
                            exampleLayout.setBackground(drawable);

                            break;
                        case MotionEvent.ACTION_UP:
                            colorPickControl.SwitchEditMode(false, null);
                            lockableScrollView.setScrollingEnabled(true);
                            oldX = null;
                            oldY = null;
                            layoutEditMode.setVisibility(View.GONE);
                            break;
                    }
                }
                return false;
            }
        };
    }

    View.OnClickListener getOnClickListenerFavoriteCheckBox() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorButton colorButton = colorPickControl.GetColorButton();
                if (colorButton != null) {
                    if (favoriteCheckBox.isChecked()) {
                        final ColorButton cloneButton = new ColorButton(context, colorButton.getColor(), colorButton.getLeftLimit(), colorButton.getRightLimit());
                        cloneButton.setOnClickListener(getOnClickListener(cloneButton));
                        cloneButton.setFavorite(true);
                        dbHelper.insertColorFavorite(colorButton.getColor());
                        colorPickControl.UpdateColorButton(cloneButton);
                        favoriteColorsGridAdapter.setSingle(favoriteColorsGridAdapter.getCount());
                        favoriteColorsGridAdapter.getCursor().close();
                        favoriteColorsGridAdapter.swapCursor(dbHelper.getCursor(dbHelper.getReadableDatabase(), DBHelper.TABLE_FAVORITES));
                        favoriteColorsGridAdapter.notifyDataSetChanged();
                    } else {
                        if (dbHelper.containsColor(colorButton.getColor())) {
                            dbHelper.removeColorFavorite(colorButton.getColor());
                            favoriteColorsGridAdapter.selected.clear();
                            favoriteColorsGridAdapter.getCursor().close();
                            favoriteColorsGridAdapter.swapCursor(dbHelper.getCursor(dbHelper.getReadableDatabase(), DBHelper.TABLE_FAVORITES));
                            favoriteColorsGridAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        };
    }
}
