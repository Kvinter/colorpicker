package com.example.ars.colorpicker;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

public class FavoriteColorsGridAdapter extends CursorAdapter {
    private Context context;
    public ArrayList<Integer> selected;
    private boolean multySelect;

    ColorButton[] getSelected() {
        ColorButton[] colorButtonsArray;
        ArrayList<ColorButton> colorButtons = new ArrayList<>();
        if(selected.size() == 0)
            return null;
        for(int select : selected)
            colorButtons.add(get(select));
        colorButtonsArray = new ColorButton[colorButtons.size()];
        colorButtons.toArray(colorButtonsArray);
        return colorButtonsArray;
    }

    ColorButton getFirstSelected() {
        if(selected.size() == 0)
            return null;
        return get(selected.get(0));
    }

    public void setSingle(int position){
        if(selected.isEmpty())
            selected.add(position);
        else
            selected.set(0, position);
    }
    public ColorButton get(int position) {
        Cursor cursor = getCursor();
        ColorButton colorButton = null;
        if (cursor.moveToPosition(position)) {
            int color = cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_COLOR));
            colorButton = new ColorButton(context, color, 0, 360);
        }
        return colorButton;
    }


    public FavoriteColorsGridAdapter(Context context, Cursor cursor, boolean multySelect) {
        super(context, cursor, 0);
        this.context = context;
        selected = new ArrayList<>();
        this.multySelect = multySelect;
    }

    public FavoriteColorsGridAdapter(Context context, Cursor cursor) {
        this(context, cursor, false);
    }

    @Override
    public long getItemId(int position) {
        return selected.contains(position) ? 1 : 0;
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.favorite_color, null, true);
        ColorHolder holder = new ColorHolder();
        holder.imageView = (ImageView) view.findViewById(R.id.image);
        holder.checked = (ImageView) view.findViewById(R.id.check);
        view.setTag(holder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ColorHolder holder = (ColorHolder) view.getTag();

        int color = cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_COLOR));

        Drawable drawable = holder.imageView.getBackground();

        int position = cursor.getPosition();
        if (selected.contains(position)) {
            holder.imageView.setSelected(true);
            holder.checked.setVisibility(View.VISIBLE);
        } else {
            holder.imageView.setSelected(false);
            holder.checked.setVisibility(View.INVISIBLE);
        }


        drawable.setColorFilter(color, PorterDuff.Mode.SRC);
        holder.imageView.setBackground(drawable);
    }

    public Integer getPosition(int color) {
        Cursor cursor = getCursor();
        if (!cursor.moveToFirst())
            return null;
        int position = 0;
        do {
            if (cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_COLOR)) == color)
                return position;
            position++;
        } while (cursor.moveToNext());
        return null;
    }

    private class ColorHolder {
        ImageView imageView;
        ImageView checked;
    }

}
