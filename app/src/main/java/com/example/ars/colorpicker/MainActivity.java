package com.example.ars.colorpicker;

import android.app.ActionBar;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private NoteAdapter noteAdapter;
    private DBHelper db;
    final static int EDIT_NOTE = 1000;
    final static int CREATE_NOTE = 2000;
    final static int OPEN_SAVE_FILE = 3000;
    final static int OPEN_LOAD_FILE = 4000;
    final static String KEY_REQUEST_CODE = "RequestCode";
    private boolean isSearch = false;

    private Cursor getCursor(Bundle savedInstanceState) {
        SQLiteDatabase readableDatabase = db.getReadableDatabase();
        if (savedInstanceState == null) {
            return db.getCursor(readableDatabase, DBHelper.TABLE_NOTES);
        } else {
            Pair<String, String[]> result = DBQueryHelper.getQueryFromBundle(savedInstanceState);
            String sortBy = DBQueryHelper.getSortByFromBundle(savedInstanceState);
            return readableDatabase.query(DBHelper.TABLE_NOTES, null, result.first, result.second, null, null, sortBy);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);
        return true;
    }

    private void saveNotesJson(String fileName) throws IOException, JSONException {
        File writeFile = new File(fileName);
        try (Writer file = new BufferedWriter(new FileWriter(writeFile))) {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < noteAdapter.getCount(); i++) {
                jsonArray.put(noteAdapter.get(i).toJson());
            }
            file.write(jsonArray.toString());
        }
    }

    private void loadNotesJson(String fileName) throws IOException, JSONException, ParseException {
        String json = getText(fileName).toString();
        try (SQLiteDatabase readableDatabase = db.getWritableDatabase()) {
            readableDatabase.delete(DBHelper.TABLE_NOTES, null, null);
            JSONArray jsonArray = new JSONArray(json);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                NoteInfo noteInfo = NoteInfo.fromJson(jsonObject);
                db.insertNote(noteInfo, readableDatabase);
            }
        }

    }

    @NonNull
    private StringBuilder getText(String fileName) throws IOException {
        File readFile = new File(fileName);
        if (!readFile.exists())
            throw new FileNotFoundException();
        StringBuilder text = new StringBuilder();
        try (FileReader file = new FileReader(readFile)) {
            BufferedReader bufferedReader = new BufferedReader(file);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                text.append(line);
                Log.i("Test", "text : " + text + " : end");
                text.append('\n');
            }
        }
        return text;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.app_bar_search:
                intent = new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(intent);
                if(isSearch)
                    finish();
                return true;
            case R.id.app_bar_save:
                intent = new Intent(getApplicationContext(), OpenFileActivity.class);
                startActivityForResult(intent, OPEN_SAVE_FILE);
                return true;
            case R.id.app_bar_load:
                intent = new Intent(getApplicationContext(), OpenFileActivity.class);
                startActivityForResult(intent, OPEN_LOAD_FILE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DBHelper(this);
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            isSearch = true;
            ActionBar actionBar = getActionBar();
            if (actionBar != null)
                actionBar.setTitle(getResources().getString(R.string.result));
        }
        noteAdapter = new NoteAdapter(this, getCursor(extras));
        final ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(noteAdapter);
        noteAdapter.notifyDataSetChanged();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), ViewNoteActivity.class);
                NoteInfo currentNoteInfo = noteAdapter.get(position);
                currentNoteInfo.setDateView(new Date().getTime());
                intent.putExtra(NoteInfo.KEY_LABEL, currentNoteInfo.getName());
                intent.putExtra(NoteInfo.KEY_TEXT, currentNoteInfo.getText());
                intent.putExtra(NoteInfo.KEY_COLOR, currentNoteInfo.getColor());
                intent.putExtra(NoteInfo.KEY_DATE_CREATE, currentNoteInfo.getDateCreate());
                intent.putExtra(NoteInfo.KEY_DATE_EDIT, currentNoteInfo.getDateEditing());
                intent.putExtra(NoteInfo.KEY_DATE_VIEW, currentNoteInfo.getDateView());
                intent.putExtra(DBHelper.COLUMN_ID, currentNoteInfo.getId());
                db.updateNote(currentNoteInfo);
                noteAdapter.getCursor().close();
                SQLiteDatabase readableDatabase = db.getReadableDatabase();
                noteAdapter.swapCursor(db.getCursor(readableDatabase, DBHelper.TABLE_NOTES));
                noteAdapter.notifyDataSetChanged();
                startActivity(intent);
            }
        });
        Button addNoteButton = (Button) findViewById(R.id.addNoteButton);
        addNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditActivity.class);
                startActivityForResult(intent, CREATE_NOTE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == EDIT_NOTE || requestCode == CREATE_NOTE) {
            NoteEditOrCreate(requestCode, resultCode, data);
        }
        if(requestCode == OPEN_SAVE_FILE){
            try {
                String path = data.getData().getPath();
                saveNotesJson(path);
                Toast.makeText(this, "Сохранено", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
                if(e.getMessage().contains("Permission denied"))
                    Toast.makeText(this, "Нет прав на запись", Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "Ошибка при записи", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "Ошибка при сериализации", Toast.LENGTH_SHORT).show();
            }
        }
        if(requestCode == OPEN_LOAD_FILE){
            try {
                String path = data.getData().getPath();
                loadNotesJson(path);
                Toast.makeText(this, "Загружен", Toast.LENGTH_SHORT).show();
                noteAdapter.getCursor().close();
                SQLiteDatabase readableDatabase = db.getReadableDatabase();
                noteAdapter.swapCursor(db.getCursor(readableDatabase, DBHelper.TABLE_NOTES));
                noteAdapter.notifyDataSetChanged();
            } catch (IOException e) {
                e.printStackTrace();
                if(e.getMessage().contains("Permission denied"))
                    Toast.makeText(this, "Нет прав на чтение", Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "Ошибка при чтении", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "Ошибка при сериализации", Toast.LENGTH_SHORT).show();
            } catch (ParseException e) {
                e.printStackTrace();
                Toast.makeText(this, "Ошибка при парсинге", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void NoteEditOrCreate(int requestCode, int resultCode, Intent data) {
        if (data == null || resultCode != RESULT_OK) return;
        String labelExtra = data.getStringExtra(NoteInfo.KEY_LABEL);
        String textExtra = data.getStringExtra(NoteInfo.KEY_TEXT);
        long createDate;
        long editDate;
        long viewDate;


        if (requestCode == EDIT_NOTE) {
            createDate = data.getLongExtra(NoteInfo.KEY_DATE_CREATE, 0);
            editDate = new Date().getTime();
            viewDate = data.getLongExtra(NoteInfo.KEY_DATE_VIEW, 0);
        } else {
            createDate = new Date().getTime();
            editDate = new Date().getTime();
            viewDate = new Date().getTime();
        }

        int colorExtra = data.getIntExtra(NoteInfo.KEY_COLOR, Color.WHITE);

        NoteInfo noteInfo = new NoteInfo(labelExtra, textExtra, colorExtra, createDate);
        noteInfo.setDateEditing(editDate);
        noteInfo.setDateView(viewDate);

        if (requestCode == EDIT_NOTE) {
            Integer id = data.getIntExtra(DBHelper.COLUMN_ID, -1);
            noteInfo.setId(id);
            db.updateNote(noteInfo);
        } else {
            db.insertNote(noteInfo);
        }

        noteAdapter.getCursor().close();
        SQLiteDatabase readableDatabase = db.getReadableDatabase();
        noteAdapter.swapCursor(db.getCursor(readableDatabase, DBHelper.TABLE_NOTES));
        noteAdapter.notifyDataSetChanged();
    }
}
