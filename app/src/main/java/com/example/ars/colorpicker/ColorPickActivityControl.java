package com.example.ars.colorpicker;

/**
 * Created by Ars on 08.05.2017.
 */
abstract class ColorPickActivityControl {
    public abstract void SwitchEditMode(boolean mode, ColorButton button);

    public abstract boolean IsEditMode();

    public abstract void UpdateColorButton(ColorButton button);

    public abstract void UpdateBackgroundColor(int color);

    public abstract ColorButton GetColorButton();
}
