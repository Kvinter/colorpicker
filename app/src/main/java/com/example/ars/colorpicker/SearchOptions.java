package com.example.ars.colorpicker;

import java.util.ArrayList;

/**
 * Created by Ars on 08.05.2017.
 */
class SearchOptions {
    public String SearchPattern;
    public String SortBy;
    public String FilterBy;
    public long DateFrom;
    public long DateTo;
    public long Date;
    public ArrayList<Integer> Color;
    public boolean Direction;

    public void setColors(ArrayList<Integer> colors)
    {
        Color = colors;

    }
}
