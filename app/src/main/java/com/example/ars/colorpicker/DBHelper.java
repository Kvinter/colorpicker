package com.example.ars.colorpicker;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.UUID;

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, "myDB", null, 1);
    }

    public static final String TABLE_FAVORITES = "favorites";
    public static final String TABLE_NOTES = "notes";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NOTES + " ("
                + COLUMN_ID             + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COLUMN_LABEL          + " NVARCHAR(36),"
                + COLUMN_TEXT           + " TEXT,"
                + COLUMN_DATE_CREATE    + " INTEGER,"
                + COLUMN_DATE_EDITING   + " INTEGER,"
                + COLUMN_DATE_VIEW      + " INTEGER,"
                + COLUMN_COLOR          + " INTEGER);");

        db.execSQL("create table " + TABLE_FAVORITES + " ("
                + COLUMN_ID     + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + COLUMN_COLOR  + " INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Cursor getCursor(SQLiteDatabase db, String tableName) {
        return db.query(tableName, null, null, null, null, null, null);
    }

    public void updateNote(NoteInfo noteInfo){

        try(SQLiteDatabase db = getWritableDatabase()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_LABEL, noteInfo.getName());
            contentValues.put(COLUMN_TEXT, noteInfo.getText());
            contentValues.put(COLUMN_COLOR, noteInfo.getColor());
            contentValues.put(COLUMN_DATE_CREATE, noteInfo.getDateCreate());
            contentValues.put(COLUMN_DATE_EDITING, noteInfo.getDateEditing());
            contentValues.put(COLUMN_DATE_VIEW, noteInfo.getDateView());
            db.update(TABLE_NOTES, contentValues, COLUMN_ID + "=?", new String[]{Integer.toString(noteInfo.getId())});
        }

    }

    public void insertNote(NoteInfo noteInfo){
        try(SQLiteDatabase db = getWritableDatabase()) {
            insertNote(noteInfo, db);
        }
    }

    public void insertNote(NoteInfo noteInfo, SQLiteDatabase db){
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_LABEL, noteInfo.getName());
        contentValues.put(COLUMN_TEXT, noteInfo.getText());
        contentValues.put(COLUMN_COLOR, noteInfo.getColor());
        contentValues.put(COLUMN_DATE_CREATE, noteInfo.getDateCreate());
        contentValues.put(COLUMN_DATE_EDITING, noteInfo.getDateEditing());
        contentValues.put(COLUMN_DATE_VIEW, noteInfo.getDateView());
        db.insert(TABLE_NOTES, null, contentValues);
    }

    public void removeColorFavorite(int color){
        try(SQLiteDatabase db = getWritableDatabase()) {
            db.delete(TABLE_FAVORITES, COLUMN_COLOR + "=?", new String[] {Integer.toString(color)});
        }
    }

    public void insertColorFavorite(int color){
        try(SQLiteDatabase db = getWritableDatabase()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_COLOR, color);
            db.insert(TABLE_FAVORITES, null, contentValues);
        }
    }

    public boolean containsColor(int color){
        try(SQLiteDatabase db = getReadableDatabase()){
            int count = db.query(TABLE_FAVORITES, null, COLUMN_COLOR + " = ?", new String[] {Integer.toString(color)}, null, null, null).getCount();
            return count > 0;
        }
    }

    public static final String COLUMN_ID = BaseColumns._ID;
    public static final String COLUMN_LABEL = "label";
    public static final String COLUMN_TEXT = "text";
    public static final String COLUMN_COLOR = "color";
    public static final String COLUMN_DATE_CREATE = "dateCreate";
    public static final String COLUMN_DATE_EDITING = "dateEditing";
    public static final String COLUMN_DATE_VIEW = "dateView";
}
