package com.example.ars.colorpicker;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;

public class OpenFileActivity extends AppCompatActivity {

    private final String defaultFileName = "itemlist.ili";
    private final String defaultPath = "/storage/emulated/0/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_file);

        final EditText editText = ((TextInputLayout) findViewById(R.id.textInputLayout)).getEditText();
        editText.setText(defaultFileName);
        Intent intent = getIntent();
        findViewById(R.id.openFileButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivityForResult(intent, 1244);
            }
        });

        findViewById(R.id.okButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Uri uri = Uri.fromFile(new File(defaultPath + editText.getText()));
                intent.setData(uri);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data == null)
            return;
        Uri uri = data.getData();
        if(uri == null)
            return;
        if(!uri.getScheme().equals("file")){
            Toast.makeText(this, "Not support type file", Toast.LENGTH_SHORT).show();
            return;
        }
        setResult(resultCode, data);
        finish();
    }
}
