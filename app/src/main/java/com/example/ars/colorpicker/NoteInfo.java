package com.example.ars.colorpicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NoteInfo {
    private String name;
    private String text;
    private int color;
    private int id;
    private long dateCreate;
    private long dateEditing;
    private long dateView;
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-DD'T'hh:mm:ss±hh:mm");
    public static SimpleDateFormat dateEditTextFormat = new SimpleDateFormat("yy/MM/dd");
    private static SimpleDateFormat simpleEditTextFormat = new SimpleDateFormat("yyyy-MM-DD hh:mm:ss");


    NoteInfo(String name, String text, int color, long dateCreate) {
        this.name = name;
        this.text = text;
        this.color = color;
        this.dateCreate = dateCreate;
        dateEditing = new Date().getTime();
        dateView = new Date().getTime();
    }

    public JSONObject toJson() throws JSONException {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put(JSON_KEY_TITLE, name);
        jsonObj.put(JSON_KEY_DESCRIPTION, text);
        jsonObj.put(JSON_KEY_COLOR, color);
        jsonObj.put(JSON_KEY_DATE_CREATED, simpleDateFormat.format(dateCreate));
        jsonObj.put(JSON_KEY_DATE_EDITED, simpleDateFormat.format(dateEditing));
        jsonObj.put(JSON_KEY_DATE_VIEWED, simpleDateFormat.format(dateView));
        return jsonObj;
    }

    public static NoteInfo fromJson(JSONObject jsonObject) throws JSONException, ParseException {
        Date dateCreated = simpleDateFormat.parse(jsonObject.getString(JSON_KEY_DATE_CREATED));
        Date dateEdited = simpleDateFormat.parse(jsonObject.getString(JSON_KEY_DATE_EDITED));
        Date dateViewed = simpleDateFormat.parse(jsonObject.getString(JSON_KEY_DATE_VIEWED));
        NoteInfo noteInfo = new NoteInfo(jsonObject.getString(JSON_KEY_TITLE), jsonObject.getString(JSON_KEY_DESCRIPTION), jsonObject.getInt(JSON_KEY_COLOR), dateCreated.getTime());
        noteInfo.setDateEditing(dateEdited.getTime());
        noteInfo.setDateView(dateViewed.getTime());

        return noteInfo;
    }
    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public int getColor() {
        return color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    final static String KEY_ID = "_id";
    final static String KEY_LABEL = "Label";
    final static String KEY_TEXT = "Text";
    final static String KEY_COLOR = "Color";
    final static String KEY_DATE_CREATE = "DateCreate";
    final static String KEY_DATE_EDIT = "DateEdit";
    final static String KEY_DATE_VIEW = "DateView";

    private final static String JSON_KEY_TITLE = "title";
    private final static String JSON_KEY_DESCRIPTION = "description";
    private final static String JSON_KEY_COLOR = "color";
    private final static String JSON_KEY_DATE_CREATED = "created";
    private final static String JSON_KEY_DATE_EDITED = "edited";
    private final static String JSON_KEY_DATE_VIEWED = "viewed";

    long getDateCreate() {
        return dateCreate;
    }

    long getDateEditing() {
        return dateEditing;
    }

    void setDateEditing(long dateEditing) {
        this.dateEditing = dateEditing;
    }

    static String getParseShort(Date source) {
        return simpleEditTextFormat.format(source);
    }

    long getDateView() {
        return dateView;
    }

    void setDateView(long dateView) {
        this.dateView = dateView;
    }
}
