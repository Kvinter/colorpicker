package com.example.ars.colorpicker;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;

public class EditActivity extends AppCompatActivity {

    private FrameLayout colorLayout;
    private EditText editTextLabel;
    private EditText editText;
    private NoteInfo noteInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        editTextLabel = (EditText) findViewById(R.id.labelEditText);
        editText = (EditText) findViewById(R.id.editText);
        ImageButton colorButton = (ImageButton) findViewById(R.id.colorButton);
        ImageButton saveButton = (ImageButton) findViewById(R.id.saveButton);

        colorLayout = (FrameLayout) findViewById(R.id.colorView);

        Integer id = null;
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        Integer requestCode = null;
        if(extras != null) {
            requestCode = extras.getInt(MainActivity.KEY_REQUEST_CODE, 0);
            if (requestCode == MainActivity.EDIT_NOTE) {
                id = extras.getInt(NoteInfo.KEY_ID);
                String label = extras.getString(NoteInfo.KEY_LABEL);
                String text = extras.getString(NoteInfo.KEY_TEXT);
                int color = extras.getInt(NoteInfo.KEY_COLOR);

                long dateCreate = extras.getLong(NoteInfo.KEY_DATE_CREATE);
                long dateEdit = extras.getLong(NoteInfo.KEY_DATE_EDIT);
                long dateView = extras.getLong(NoteInfo.KEY_DATE_VIEW);
                noteInfo = new NoteInfo(label, text, color, dateCreate);
                noteInfo.setDateEditing(dateEdit);
                noteInfo.setDateView(dateView);
                editTextLabel.setText(label);
                editText.setText(text);
                colorLayout.setBackgroundColor(color);
            }
        }

        final Integer finalId = id;
        final Integer finalRequestCode = requestCode;
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                String label = editTextLabel.getText().toString();
                String text = editText.getText().toString();
                String emptyString = getResources().getString(R.string.empty);
                if(label.isEmpty())
                    label = emptyString;
                if(text.isEmpty())
                    text = emptyString;
                if(finalRequestCode != null &&
                        finalRequestCode == MainActivity.EDIT_NOTE) {
                    intent.putExtra(NoteInfo.KEY_ID, finalId);
                }
                intent.putExtra(NoteInfo.KEY_LABEL, label);
                intent.putExtra(NoteInfo.KEY_TEXT, text);
                intent.putExtra(NoteInfo.KEY_COLOR, getLayoutColor());
                if(noteInfo != null){
                    intent.putExtra(NoteInfo.KEY_DATE_CREATE, noteInfo.getDateCreate());
                    intent.putExtra(NoteInfo.KEY_DATE_EDIT, noteInfo.getDateEditing());
                    intent.putExtra(NoteInfo.KEY_DATE_VIEW, noteInfo.getDateView());
                }
                setResult(RESULT_OK, intent);
                finish();
            }
        });


        colorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ColorPickActivity.class);
                intent.putExtra(NoteInfo.KEY_COLOR, getLayoutColor());
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data == null) return;
        int color = data.getIntExtra(NoteInfo.KEY_COLOR, Color.WHITE);
        colorLayout.setBackgroundColor(color);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(NoteInfo.KEY_COLOR, getLayoutColor());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState.containsKey(NoteInfo.KEY_COLOR))
            colorLayout.setBackgroundColor(savedInstanceState.getInt(NoteInfo.KEY_COLOR));
    }

    private int getLayoutColor()
    {
        return ((ColorDrawable)colorLayout.getBackground()).getColor();
    }
}
