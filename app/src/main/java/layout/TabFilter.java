package layout;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.ars.colorpicker.ColorButton;
import com.example.ars.colorpicker.DBHelper;
import com.example.ars.colorpicker.FavoriteColorsGridAdapter;
import com.example.ars.colorpicker.MainActivity;
import com.example.ars.colorpicker.NoteInfo;
import com.example.ars.colorpicker.R;
import com.example.ars.colorpicker.SearchActivity;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class TabFilter extends Fragment {


    public View view;
    public Button date;
    public Button dateFrom;
    public Button dateTo;
    private FavoriteColorsGridAdapter favoriteColorsGridAdapter;
    public Integer color;
    private RadioButton filterCreate;
    private RadioButton filterEdit;
    private RadioButton filterView;

    public static final String FILTER_TYPE_CREATE = "create";
    public static final String FILTER_TYPE_EDIT = "edit";
    public static final String FILTER_TYPE_VIEW = "view";
    public TabFilter() {
    }

    public static TabSearch newInstance() {
        TabSearch fragment = new TabSearch();
        return fragment;
    }

    public String getTypeFilter() {
        if (filterCreate.isChecked())
            return DBHelper.COLUMN_DATE_CREATE;
        if (filterEdit.isChecked())
            return DBHelper.COLUMN_DATE_EDITING;
        if (filterView.isChecked())
            return DBHelper.COLUMN_DATE_VIEW;
        return null;
    }

    public ArrayList<Integer> getColors()
    {
        ArrayList<Integer> colros = new ArrayList<>();
        for (int index : favoriteColorsGridAdapter.selected)
            colros.add(favoriteColorsGridAdapter.get(index).getColor());
        return colros;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final DBHelper dbHelper = new DBHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        view = inflater.inflate(R.layout.fragment_tab_filter, container, false);
        date = (Button) view.findViewById(R.id.date);
        dateFrom = (Button) view.findViewById(R.id.dateFrom);
        dateTo = (Button) view.findViewById(R.id.dateTo);
        filterCreate = (RadioButton) view.findViewById(R.id.radioButtonCreate);
        filterEdit = (RadioButton) view.findViewById(R.id.radioButtonEdit);
        filterView = (RadioButton) view.findViewById(R.id.radioButtonView);
        GridView grid = (GridView) view.findViewById(R.id.gridColors);
        grid.setColumnWidth((int) (getResources().getDimension(R.dimen.colorPickMiniature) + getResources().getDimension(R.dimen.colorPickMargin) * 2));
        Cursor c = db.query(DBHelper.TABLE_FAVORITES, null, null, null, null, null, null);
        favoriteColorsGridAdapter = new FavoriteColorsGridAdapter(getContext(), c);

        if (savedInstanceState != null) {
            date.setText(savedInstanceState.getString(SearchActivity.KEY_DATE));
            dateFrom.setText(savedInstanceState.getString(SearchActivity.KEY_DATE_FROM));
            dateTo.setText(savedInstanceState.getString(SearchActivity.KEY_DATE_TO));
            favoriteColorsGridAdapter.selected = savedInstanceState.getIntegerArrayList(SearchActivity.KEY_CHECKED_COLORS);
            switch (savedInstanceState.getString(SearchActivity.KEY_TYPE_FILTER)){
                case FILTER_TYPE_CREATE:
                    filterCreate.setChecked(true);
                    break;
                case FILTER_TYPE_EDIT:
                    filterEdit.setChecked(true);
                    break;
                case FILTER_TYPE_VIEW:
                    filterView.setChecked(true);
                    break;
            }
        }

        setDateDialog(date);
        setDateDialog(dateFrom);
        setDateDialog(dateTo);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!favoriteColorsGridAdapter.selected.contains(position)) {
                    favoriteColorsGridAdapter.selected.add(position);
                    color = favoriteColorsGridAdapter.get(position).getColor();
                    favoriteColorsGridAdapter.notifyDataSetChanged();
                } else {
                    favoriteColorsGridAdapter.selected.remove((Object) position);
                    color = null;
                    favoriteColorsGridAdapter.notifyDataSetChanged();
                }
            }
        });
        grid.setEmptyView(view.findViewById(R.id.emptyTextView));
        grid.setNumColumns(GridView.AUTO_FIT);
        grid.setAdapter(favoriteColorsGridAdapter);

        return view;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SearchActivity.KEY_DATE, date.getText().toString());
        outState.putString(SearchActivity.KEY_DATE_FROM, dateFrom.getText().toString());
        outState.putString(SearchActivity.KEY_DATE_TO, dateTo.getText().toString());
        outState.putIntegerArrayList(SearchActivity.KEY_CHECKED_COLORS, favoriteColorsGridAdapter.selected);
        String typeFilter = "";
        if(filterCreate.isChecked())
            typeFilter = FILTER_TYPE_CREATE;
        if(filterEdit.isChecked())
            typeFilter = FILTER_TYPE_EDIT;
        if(filterView.isChecked())
            typeFilter = FILTER_TYPE_VIEW;
        outState.putString(SearchActivity.KEY_TYPE_FILTER, typeFilter);
    }

    private void setDateDialog(final Button editText) {
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        editText.setText(dayOfMonth + "/" + month + "/" + year);
                    }
                };
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                ArrayList<Integer> date = tryGetDate(editText.getText().toString());
                if(date != null)
                {
                    year = date.get(0);
                    month = date.get(1);
                    day = date.get(2);
                }
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), onDateSetListener, year, month, day);
                datePickerDialog.show();
            }
        });
    }

    private ArrayList<Integer> tryGetDate(String text) {
        ArrayList<Integer> date = new ArrayList<>();
        for (String num : text.split("/")) {
            try {
                date.add(Integer.parseInt(num));
            } catch (Exception e) {
                return null;
            }
        }
        if (date.size() == 3)
            return date;
        else
            return null;
    }

}
