package layout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RadioButton;

import com.example.ars.colorpicker.DBHelper;
import com.example.ars.colorpicker.R;
import com.example.ars.colorpicker.SearchActivity;

public class TabSearch extends Fragment {

    public TextInputLayout searchEditText;
    private RadioButton filterCreate;
    private RadioButton filterEdit;
    private RadioButton filterView;
    public CheckBox direction;

    public String getTypeSearch() {
        if (filterCreate.isChecked())
            return DBHelper.COLUMN_DATE_CREATE;
        if (filterEdit.isChecked())
            return DBHelper.COLUMN_DATE_EDITING;
        if (filterView.isChecked())
            return DBHelper.COLUMN_DATE_VIEW;
        return null;
    }

    public TabSearch() {
    }

    public static TabSearch newInstance() {
        TabSearch fragment = new TabSearch();
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SearchActivity.KEY_PATTERN, searchEditText.getEditText().getText().toString());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_search, container, false);
        searchEditText = (TextInputLayout) view.findViewById(R.id.searchInput);
        direction = (CheckBox) view.findViewById(R.id.direction);
        filterCreate = (RadioButton) view.findViewById(R.id.radioButtonCreate);
        filterEdit = (RadioButton) view.findViewById(R.id.radioButtonEdit);
        filterView = (RadioButton) view.findViewById(R.id.radioButtonView);

        if(savedInstanceState != null)
            searchEditText.getEditText().setText(savedInstanceState.getString(SearchActivity.KEY_PATTERN));
        return view;
    }

}
